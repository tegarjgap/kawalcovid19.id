import * as React from 'react';
import styled from '@emotion/styled';
import Link from 'next/link';

import { Text, Box, Stack } from 'components/ui-core';
import Content from './Content';
import Column from './Column';

const FooterElement = Content.withComponent('footer');

const Root = styled(FooterElement)`
  padding: 48px 24px 84px;
`;

const TextLink = styled(Text)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const Footer: React.FC = () => {
  return (
    <Root noPadding noFlex>
      <Column>
        <Stack
          spacing="sm"
          paddingTop="md"
          borderTopWidth="1px"
          borderTopStyle="solid"
          borderTopColor="accents01"
        >
          <Box>
            <Text variant={300} as="p" color="accents05">
              Hak Cipta &copy; 2020 &middot; kawalcovid19.id
            </Text>
          </Box>
          <Box>
            <Link href="/kebijakan-privasi" passHref>
              <TextLink
                variant={300}
                as="a"
                display="inline-block"
                fontWeight={700}
                color="accents05"
              >
                Kebijakan Privasi
              </TextLink>
            </Link>
          </Box>
        </Stack>
      </Column>
    </Root>
  );
};

export default Footer;
