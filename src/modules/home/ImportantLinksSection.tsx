import * as React from 'react';
import styled from '@emotion/styled';

import { Box, Heading, themeProps, UnstyledAnchor, Stack } from 'components/ui-core';
import { logEventClick } from 'utils/analytics';

import importantLinks from 'content/importantLinks.json';

const CardLink = styled(UnstyledAnchor)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 3 - 48px), 1fr)
    );
  }
`;

const ImportantLinksSection: React.FC = () => {
  return (
    <Stack spacing="xl" mb="xxl">
      <Heading variant={800} as="h2">
        Pranala-pranala Penting
      </Heading>
      <GridWrapper>
        {importantLinks.map(({ name, link }) => (
          <CardLink
            key={name}
            href={link}
            target="_blank"
            rel="noopener noreferrer"
            onClick={() => logEventClick(name)}
          >
            <Box bg="accents01" borderRadius={8} height="100%">
              <Box p="md">
                <Heading variant={600} as="h3">
                  {name}
                </Heading>
              </Box>
            </Box>
          </CardLink>
        ))}
      </GridWrapper>
    </Stack>
  );
};

export default ImportantLinksSection;
