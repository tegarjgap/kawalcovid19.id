import * as React from 'react';
import { Box, Text } from 'components/ui-core';
import { WordPressUser } from 'types/wp';
import styled from '@emotion/styled';
import Link from 'next/link';
import { logEventClick } from 'utils/analytics';

interface PostAuthorProps {
  author: WordPressUser;
}

const IndexAvatar = styled('img')`
  width: 48px;
  height: 48px;
  object-fit: cover;
`;

const AuthorLink = styled(Text)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const PostAuthor: React.FC<PostAuthorProps> = ({ author }) => {
  const { name, avatar_urls, slug, description } = author;
  return (
    <Box display="flex" flexDirection="row" alignItems="center">
      <Box
        size={48}
        minWidth={48}
        borderWidth={1}
        borderStyle="solid"
        borderRadius={48}
        overflow="hidden"
      >
        <IndexAvatar alt={`${name}'s Avatar`} src={avatar_urls['48']} />
      </Box>
      <Box display="flex" flexDirection="column" flex="1 1 auto" marginLeft="sm">
        <Link href="/author/[slug]" as={`/author/${slug}`} passHref>
          <AuthorLink
            as="a"
            fontWeight="600"
            mb="xxs"
            onClick={() => logEventClick(`/author/${slug}`)}
          >
            {name}
          </AuthorLink>
        </Link>
        {description && <Text variant={300}>{description}</Text>}
      </Box>
    </Box>
  );
};

export default PostAuthor;
