import * as React from 'react';
import { NextPage, GetStaticProps, GetStaticPaths } from 'next';

import { wp } from 'utils/api';
import { WordPressPostIndex, WordPressUser } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { AuthorHeader, PostIndexCard } from 'modules/posts-index';
import { ArticlesListGrid } from 'modules/home';
import { Box, Text } from 'components/ui-core';

interface IndexPageProps {
  user?: WordPressUser;
  posts?: WordPressPostIndex[];
  errors?: string;
}

const Section = Content.withComponent('section');

const AuthorIndexPage: NextPage<IndexPageProps> = ({ user, posts }) => (
  <PageWrapper>
    {user && (
      <AuthorHeader
        title={user.name}
        description={user.description}
        avatar={user.avatar_urls['96']}
      />
    )}
    <Section>
      <Column>
        {posts ? (
          <Box mb="xxl">
            <ArticlesListGrid>
              {posts.map(post => (
                <PostIndexCard key={post.slug} post={post} author={user} hasExcerpt />
              ))}
            </ArticlesListGrid>
          </Box>
        ) : (
          <Box my="xxl" textAlign="center">
            <Text color="accents06">Tidak ada konten ditemukan.</Text>
          </Box>
        )}
      </Column>
    </Section>
  </PageWrapper>
);

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    if (params) {
      const { slug } = params;

      const user = await wp('wp/v2/users', {
        slug,
        _fields: 'id,name,description,slug,avatar_urls',
      });

      if (user && user[0]) {
        const unfilteredPosts = await wp<WordPressPostIndex[]>('wp/v2/posts', {
          author: user[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
        });

        if (Array.isArray(unfilteredPosts)) {
          const posts = unfilteredPosts.filter(post => post.type === 'post');
          return { props: { user: user[0], posts } };
        }

        return { props: { user: user[0], posts: [] } };
      }
    }

    throw new Error('Failed to fetch category');
  } catch (err) {
    return { props: { errors: err.message } };
  }
};

export const getStaticPaths: GetStaticPaths = async () => {
  const pagesList = await wp<WordPressUser[]>('wp/v2/users', {
    per_page: 100,
  });

  const paths =
    pagesList && pagesList.length
      ? pagesList.map(page => {
          return { params: { slug: page.slug } };
        })
      : [];

  return {
    fallback: true,
    paths,
  };
};

export default AuthorIndexPage;
